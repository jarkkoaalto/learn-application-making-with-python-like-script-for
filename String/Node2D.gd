extends Node2D

# Declare member variables here. Examples:
var label_1 = Label.new()
var btn_1 = Button.new()
var inp_field = TextEdit.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	label_1.rect_position = Vector2(30,30)
	.add_child(label_1)
	btn_1.rect_position = Vector2(30,120)
	btn_1.text ="Click for characters no "
	.add_child(btn_1)
	btn_1.connect("button_down", self, "Button_press")
	inp_field.rect_position=Vector2(30,60)
	inp_field.rect_size=Vector2(180,60)
	inp_field.text = "simple "
	.add_child(inp_field)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func Button_press():
	label_1.text = "In text - Characters: " + str(inp_field.text.length())
