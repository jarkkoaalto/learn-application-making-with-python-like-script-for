extends KinematicBody2D

const GRAVITY = 300.0
const WALK_SPEED = 100
var velocity = Vector2()
var jump_speed = 300

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	if Input.is_action_pressed("ui_left"):
		velocity.x = -WALK_SPEED
		$icon/AnimationPlayer.play("Blink right")
	elif Input.is_action_pressed("ui_right"):
		velocity.c = WALK_SPEED
		$icon/AnimationPlayer.play("Blink left")
	elif is_on_floor() && Input.is_action_just_pressed("ui_up"):
		print("on floor")
		velocity.y=-jump_speed
	else:
		velocity.x = 0
	move_and_slide(velocity, Vector2(0,-1))		

