extends Node2D

# Declare member variables here. Examples:
var a = 2
var b = 9
var c

# Called when the node enters the scene tree for the first time.
func _ready():
	c = a+b
	print("Result adding " + str(a+b))
	print ("Result adding " + str(a)+ " + " + str(b) + " = " + str(c))
	multi(a,b)
	div(a,b)

func multi(a,b):
	c = a*b
	print ("Result multiplying " + str(a)+ " * " + str(b) + " = " + str(c))

func div(a,b):
	c = b/a
	print ("Result Division " + str(a)+ " / " + str(b) + " = " + str(c))

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
