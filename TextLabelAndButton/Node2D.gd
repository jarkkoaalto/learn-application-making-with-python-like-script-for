extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var label_1 = Label.new()

var btn_1 = Button.new()

# Called when the node enters the scene tree for the first time.
func _ready():
	label_1.text = ""
	label_1.rect_position = Vector2(30,30)
	.add_child(label_1)

	btn_1.rect_position = Vector2(30, 60)
	btn_1.text = "Click to read"
	.add_child(btn_1)
	btn_1.connect("button_down", self, "Button_press")

func Button_press():
	label_1.text = "Game On"