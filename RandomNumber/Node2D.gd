extends Node2D

# Declare member variables here. Examples:
var label_1 = Label.new()
var btn_1 = Button.new()
var rnd_number


# Called when the node enters the scene tree for the first time.
func _ready():
	label_1.text=""
	label_1.rect_position = Vector2(30,30)
	.add_child(label_1)
	btn_1.rect_position = Vector2(30,60)
	btn_1.text = "Click to read"
	.add_child(btn_1)
	btn_1.connect("button_down", self, "Button_press")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func Button_press():
	create_random()
	label_1.text="Random number is: " + str(rnd_number)
	
	
func create_random():
	randomize()
	rnd_number = randi()%12+1
#	pass
